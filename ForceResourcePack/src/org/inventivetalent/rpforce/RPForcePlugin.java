/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.rpforce;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.MetricsLite;

import java.net.MalformedURLException;
import java.net.URL;

public class RPForcePlugin extends JavaPlugin {

	public static RPForcePlugin instance;

	public static String PACK = "http://path/to/resource/pack.zip";

	public static String MSG_SEND        = "";
	public static String MSG_LOADED      = "";
	public static String MSG_ACCEPTED    = "";
	public static String MSG_FAILED_LOAD = "";
	public static String MSG_DECLINED    = "";
	public static String MSG_DENY        = "";

	public static boolean DENY_ON_FAIL    = true;
	public static boolean KICK_ON_DENY    = false;
	public static boolean COMMAND_ENABLED = false;
	public static String  COMMAND         = "/";
	public static boolean CONSOLE_LOG     = true;

	public static boolean DENY_INTERACT        = false;
	public static boolean DENY_INTERACT_ENTITY = false;
	public static boolean DENY_DAMAGE          = false;
	public static boolean DENY_CHANGE_WORLD    = false;
	public static boolean DENY_CHAT            = false;
	public static boolean DENY_BUILD           = false;
	public static boolean DENY_MOVE            = false;

	public static boolean ON_JOIN = true;

	public static String RP_HASH = "rp_force";

	@Override
	public void onEnable() {
		instance = this;

		if (!Bukkit.getPluginManager().isPluginEnabled("ResourcePackApi")) {
			getLogger().severe("****************************************");
			getLogger().severe(" ");
			getLogger().severe(" This plugin depends on ResourcePackApi ");
			getLogger().severe(" http://www.spigotmc.org/resources/2397 ");
			getLogger().severe(" ");
			getLogger().severe("****************************************");

			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		saveDefaultConfig();
		loadConfig();

		Bukkit.getPluginManager().registerEvents(new ResourcePackHandler(), this);
		if (!KICK_ON_DENY) {
			Bukkit.getPluginManager().registerEvents(new DenyListener(), this);
		}
		Bukkit.getPluginManager().registerEvents(new OnListener(), this);

		try {
			MetricsLite metrics = new MetricsLite(this);
			if (metrics.start()) {
				getLogger().info("Metrics started");
			}
		} catch (Exception e) {
		}
	}

	void loadConfig() {
		reloadConfig();

		PACK = parseLink(getConfig().getString("link", PACK));

		MSG_SEND = colorize(getConfig().getString("message.send", MSG_SEND));
		MSG_LOADED = colorize(getConfig().getString("message.loaded", MSG_LOADED));
		MSG_ACCEPTED = colorize(getConfig().getString("message.accepted", MSG_ACCEPTED));
		MSG_FAILED_LOAD = colorize(getConfig().getString("message.failed_load", MSG_FAILED_LOAD));
		MSG_DECLINED = colorize(getConfig().getString("message.declined", MSG_DECLINED));

		MSG_DENY = colorize(getConfig().getString("deny.message", MSG_DENY));

		DENY_ON_FAIL = getConfig().getBoolean("deny_on_fail");
		KICK_ON_DENY = getConfig().getBoolean("kick_on_deny");
		COMMAND_ENABLED = getConfig().getBoolean("command_on_deny.enabled");
		COMMAND = getConfig().getString("command_on_deny.command", COMMAND);
		if (COMMAND.startsWith("/")) {
			COMMAND = COMMAND.substring(1);
		}
		CONSOLE_LOG = getConfig().getBoolean("console_log");

		DENY_INTERACT = getConfig().getBoolean("deny.interact");
		DENY_INTERACT_ENTITY = getConfig().getBoolean("deny.interact_entity");
		DENY_DAMAGE = getConfig().getBoolean("deny.damage");
		DENY_CHANGE_WORLD = getConfig().getBoolean("deny.change_world");
		DENY_CHAT = getConfig().getBoolean("deny.chat");
		DENY_BUILD = getConfig().getBoolean("deny.build");
		DENY_MOVE = getConfig().getBoolean("deny.move");

		ON_JOIN = getConfig().getBoolean("set_on.join");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (command.getLabel().equalsIgnoreCase("forceresourcepack")) {
			if (args.length == 0) {
				if (sender.hasPermission("forcerp.reload")) {
					sender.sendMessage("§b/forcerp reload");
				}
				if (sender.hasPermission("forcerp.set")) {
					sender.sendMessage("§b/forcerp set <player>");
				}
				if (sender.hasPermission("forcerp.setall")) {
					sender.sendMessage("§b/forcerp setall");
				}
				return true;
			}
			if (args.length >= 1) {
				if ("reload".equalsIgnoreCase(args[0])) {
					if (sender.hasPermission("forcerp.reload")) {
						loadConfig();
						sender.sendMessage("§aReloaded configuration");
						return true;
					}
				}
				if ("set".equalsIgnoreCase(args[0])) {
					if (sender.hasPermission("forcerp.set")) {
						if (args.length == 1) {
							sender.sendMessage("§c/forcerp set <player>");
							return false;
						}
						Player target = Bukkit.getPlayer(args[1]);
						if (target == null || !target.isOnline()) {
							sender.sendMessage("§cPlayer not found");
							return false;
						}
						ResourcePackHandler.setPack(target);
						sender.sendMessage("§aSent " + target.getName() + " the resource pack");
						return true;
					}
				}
				if ("setall".equalsIgnoreCase(args[0])) {
					if (sender.hasPermission("forcerp.setall")) {
						for (Player p : Bukkit.getOnlinePlayers()) {
							ResourcePackHandler.setPack(p);
						}
						sender.sendMessage("§aSent the resource pack to all online players");
						return true;
					}
				}
			}
		}
		return false;
	}

	public static void sendMessage(Player p, String message) {
		if (message != null && !message.isEmpty()) {
			p.sendMessage(message);
		}
	}

	private String parseLink(String link) {
		if (link == null || link.isEmpty()) { return link; }
		URL url = null;
		try {
			url = new URL(link);
		} catch (MalformedURLException e) {
			getLogger().warning("Invalid resource pack URL: " + link);
		}
		if (url == null) { return link; }
		return url.toString();
	}

	String colorize(String s) {
		return s.replaceAll("(&([a-fk-or0-9]))", "\u00A7$2");
	}

}
