/*
 * Copyright 2015-2016 inventivetalent. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package org.inventivetalent.rpforce;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.metadata.FixedMetadataValue;
import org.inventivetalent.rpapi.ResourcePackAPI;
import org.inventivetalent.rpapi.ResourcePackStatusEvent;

public class ResourcePackHandler implements Listener {

	public static void setPack(Player p) {
		RPForcePlugin.sendMessage(p, RPForcePlugin.MSG_SEND);
		ResourcePackAPI.setResourcepack(p, RPForcePlugin.PACK, RPForcePlugin.RP_HASH);
	}

	@EventHandler
	public void onResourcePackStatus(final ResourcePackStatusEvent e) {
		if (!RPForcePlugin.RP_HASH.equals(e.getHash())) { return; }

		if (RPForcePlugin.CONSOLE_LOG) {
			RPForcePlugin.instance.getLogger().info("Result of " + e.getPlayer().getName() + ": " + e.getStatus());
		}

		boolean accepted = true;
		String message = "";
		switch (e.getStatus()) {
			case ACCEPTED:
				message = RPForcePlugin.MSG_ACCEPTED;
				break;
			case SUCCESSFULLY_LOADED:
				message = RPForcePlugin.MSG_LOADED;
				break;
			case DECLINED:
				message = RPForcePlugin.MSG_DECLINED;
				accepted = false;
				break;
			case FAILED_DOWNLOAD:
				message = RPForcePlugin.MSG_FAILED_LOAD;
				if (RPForcePlugin.DENY_ON_FAIL) { accepted = false; }
				break;
			default:
				break;
		}

		RPForcePlugin.sendMessage(e.getPlayer(), message);

		if (accepted) {
			e.getPlayer().setMetadata("rp_force_accepted", new FixedMetadataValue(RPForcePlugin.instance, true));
		} else {
			e.getPlayer().removeMetadata("rp_force_accepted", RPForcePlugin.instance);
			if (RPForcePlugin.COMMAND_ENABLED) {
				final String command = RPForcePlugin.COMMAND.replace("%player%", e.getPlayer().getName());
				Bukkit.getScheduler().runTaskLater(RPForcePlugin.instance, new Runnable() {
					@Override
					public void run() {
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
					}
				}, 5);
			}
			if (RPForcePlugin.KICK_ON_DENY) {
				final String msg = message;
				Bukkit.getScheduler().runTask(RPForcePlugin.instance, new Runnable() {

					@Override
					public void run() {
						e.getPlayer().kickPlayer(msg);
					}
				});
			}
		}
	}

}
